# Documentação webhook

Existem dois tipo de webhook: vendas e contratos.

No caso de webhook de vendas, o tipo do evento (type) é "sale" e o evento é "saleUpdated" indicando que a transação teve seu status alterado.

Campos:

**oldStatus**\
Descrição: indica qual o status anterior da venda.
Valores possíveis: created, paid, waiting_payment

**currentStatus**\
Descrição: indica o status atual da venda.
Valores possíveis: paid, refused, refunded, chargedback, waiting_payment

**product.affiliation_proposal**\
Descrição: permite que o afiliado possa gerar propostas.

**product.allow_proposal**\
Descrição: permite ao produto ter propostas

**product.is_active**\
Descrição: indica se o produto está ativo ou não.

**product.thank_you_page**\
Descrição: página que o usuário será redirecionado após comprar o produto.

**product.affiliation_public**\
Descrição: permite que o produto em questão possa ser exibido na vitrine para que os afiliados possam solicitar afiliação.

**product.affiliation_approbation**\
Descrição: permite que as solicitações de afiliação passem pro aprovação do vendedor ou não.

**product.method**\
Descrição: método de pagamento permitido para este produto.
Valores possíveis: CREDIT_CARD, TWO_CREDIT_CARDS, BOLETO, PIX,PAYPAL (ou todos eles)

**product.type**\
Descrição: indica qual o tipo do produto: simples ou assinatura.
Valores possíveis: TRANSACTIOM, SUBSCRIPTION

**sale.coupon**\
Descrição: indica qual e o cupom que foi aplicado na venda.
Valores possíveis: null, objeto

**saleMetas**\
Descrição: Metas da venda, se houver.
Valores possíveis: [], Array de Objeto

```json
{
   "oldStatus":"",
   "currentStatus":"",
   "type":"sale",
   "event":"saleUpdated",
   "product":{
      "affiliation_proposal":1,
      "description":"",
      "allow_proposal":0,
      "created_at":"",
      "is_active":1,
      "period":0,
      "updated_at":"",
      "thank_you_page":"",
      "amount":99,
      "id":0,
      "affiliation_public":1,
      "affiliation_approbation":1,
      "proposal_minimum":0,
      "type":"",
      "method":"",
      "name":""
   },
   "sale":{
      "status":"paid",
      "seller_id":0,
      "created_at":"",
      "updated_at":"",
      "method":"",
      "amount":99,
      "client_id":0,
      "installments":1,
      "type":"",
      "id":0,
     "coupon": {
       "id": 0,
       "seller_id": 0,
       "name": "MEUCUPOM",
       "type": "AMOUNT",
       "amount": 10.0,
       "only_products": 0,
       "limit": 5,
       "due_date": "2021-09-24 12:55:00",
       "created_at": "2021-09-24 12:49:39",
       "updated_at": "2021-09-24 12:51:13",
       "is_active": 0,
       "deleted_at": "2021-09-24 12:51:13"
     }
   },
   "seller":{
      "cellphone":"",
      "email":"",
      "name":"",
      "id":0
   },
   "client":{
      "city":"",
      "neighborhood":"",
      "name":"",
      "cellphone":"",
      "updated_at":"",
      "created_at":"",
      "number":"",
      "zipcode":"",
      "email":"",
      "street":" ",
      "cpf_cnpj":"",
      "complement":"",
      "uf":"",
      "id":0
   },
  "saleMetas": [
    {
      "id": 0,
      "sale_id": 0,
      "meta_key": "",
      "meta_value": "",
      "created_at": "2021-09-24 18:10:30",
      "updated_at": "2021-09-24 18:10:30"
    },
    {
      "id": 0,
      "sale_id": 0,
      "meta_key": "",
      "meta_value": "",
      "created_at": "2021-09-24 18:10:30",
      "updated_at": "2021-09-24 18:10:30"
    }
  ]
}
```

Webhook de contratos:

Campos:

**oldStatus**\
Descrição: indica qual o status anterior do contrato.
Valores possíveis: created, paid, trialing

**currentStatus**\
Descrição: indica o status atual do contrato.
Valores possíveis: paid, trialing, pending_payment, unpaid, canceled

**contract.start_date**\
Descrição: data de início do contrato.

**contract.current_period_end**\
Descrição: data do vencimento da mensalidade atual.

**productMetas**\
Descrição: metas do produto, se houver.

**proposalMetas**\
Descrição: metas da proposta, se houver.

**currentSale.coupon**\
Descrição: indica qual e o cupom que foi aplicado na venda.
Valores possíveis: null, objeto

**saleMetas**\
Descrição: Metas da venda, se houver.
Valores possíveis: [], Array de Objeto



```json
{
   "oldStatus":"",
   "currentStatus":"",
   "type":"contract",
   "event":"contractUpdated",
   "product":{
      "affiliation_proposal":1,
      "description":"",
      "allow_proposal":0,
      "created_at":"",
      "is_active":1,
      "period":0,
      "updated_at":"",
      "thank_you_page":"",
      "amount":0,
      "id":0,
      "affiliation_public":1,
      "affiliation_approbation":1,
      "proposal_minimum":0,
      "type":"",
      "method":"",
      "name":""
   },
   "currentSale":{
      "status":"paid",
      "seller_id":0,
      "created_at":"",
      "updated_at":"",
      "method":"",
      "amount":0,
      "client_id":0,
      "installments":1,
      "type":"",
      "id":0,
     "coupon": {
       "id": 0,
       "seller_id": 0,
       "name": "MEUCUPOM",
       "type": "AMOUNT",
       "amount": 10.0,
       "only_products": 0,
       "limit": 5,
       "due_date": "2021-09-24 12:55:00",
       "created_at": "2021-09-24 12:49:39",
       "updated_at": "2021-09-24 12:51:13",
       "is_active": 0,
       "deleted_at": "2021-09-24 12:51:13"
     }
   },
   "contract":{
      "start_date":"",
      "created_at":"",
      "updated_at":"",
      "status":"",
      "current_period_end":"",
      "id":0
   },
   "seller":{
      "cellphone":"",
      "email":"",
      "name":"",
      "id":0
   },
   "affiliate":{
      "cellphone":"",
      "email":"",
      "name":"",
      "id":0
   },
   "client":{
      "city":"",
      "neighborhood":"",
      "name":"",
      "cellphone":"",
      "updated_at":"",
      "created_at":"",
      "number":"",
      "zipcode":"",
      "email":"",
      "street":" ",
      "cpf_cnpj":"",
      "complement":"",
      "uf":"",
      "id":0
   },
   "productMetas":[
     {
      "key":"",
      "value":"",
      "id":0
   	 }
   ],
   "proposalMetas":[
     {
      "key":"",
      "value":"",
      "id":0
   	 }
   ],
  "saleMetas": [
    {
      "id": 0,
      "sale_id": 0,
      "meta_key": "",
      "meta_value": "",
      "created_at": "2021-09-24 18:10:30",
      "updated_at": "2021-09-24 18:10:30"
    },
    {
      "id": 0,
      "sale_id": 0,
      "meta_key": "",
      "meta_value": "",
      "created_at": "2021-09-24 18:10:30",
      "updated_at": "2021-09-24 18:10:30"
    }
  ]
}
```

**Objeto do evento "abandono de carrinho"**

**lead.step**\
Descrição: Qual passo o cliente parou no checkout.\
Valores possíveis: 1, 2, 3\
1 - dados pessoas, 2 - dados de endereço (se houver), 3 - dados de pagamento

```json
"type": "lead",
"event": "checkoutAbandoned",
"lead":{
   "city":"",
   "neighborhood":"",
   "name":"",
   "cellphone":"",
   "updated_at":"",
   "created_at":"",
   "number":"",
   "zipcode":"",
   "email":"",
   "street":" ",
   "cpf_cnpj":"",
   "complement":"",
   "uf":"",
   "step": 1,
   "id":0
},
"product":{
   "affiliation_proposal":1,
   "description":"",
   "allow_proposal":0,
   "created_at":"",
   "is_active":1,
   "period":0,
   "updated_at":"",
   "thank_you_page":"",
   "amount":0,
   "id":0,
   "affiliation_public":1,
   "affiliation_approbation":1,
   "proposal_minimum":0,
   "type":"",
   "method":"",
   "name":""
},
"seller":{
   "cellphone":"",
   "email":"",
   "name":"",
   "id":0
},
"affiliate":{
   "cellphone":"",
   "email":"",
   "name":"",
   "id":0
},
"productMetas":[
   {
   "key":"",
   "value":"",
   "id":0
      }
],
"proposalMetas":[
   {
   "key":"",
   "value":"",
   "id":0
      }
],

```



